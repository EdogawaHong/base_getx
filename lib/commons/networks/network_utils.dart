import 'dart:io';

import 'package:base/commons/constant/constance.dart';
import 'package:base/commons/networks/RestClient.dart';
import 'package:base/commons/networks/services.dart';
import 'package:base/commons/utils/shared_pref.dart';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class NetworkUtils {
  // ignore: non_constant_identifier_names

  static Future<String> getTOKEN() async {
    return await SharedPref.read(Constance.token);
  }

  static String getBaseUrl() {
    return AppServices.base_url;
  }

  static late RestClient client;

  static initClientHttp() async {
    var dio = await NetworkUtils.getDioClientToken();
    client = RestClient(dio);
  }

  static Future<Dio> getDioClientToken() async {
    String token = Constance.tokenKey;
    print("Token Request ###### = $token");
    token = await getTOKEN();
    final dio = Dio(BaseOptions(
        baseUrl: getBaseUrl(),
        contentType: "application/json")); // Provide a dio instance
    dio.options.headers["Authorization1"] ="$token";
    //"Bearer $token"; // config your dio headers globally
    dio.interceptors.add(
      PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true,
      ),
    );
    return dio;
  }

  static Dio getDioClient() {
    final dio = Dio(
      BaseOptions(
        baseUrl: AppServices.base_url,
        contentType: "application/json",
      ),
    ); // Provide a dio instance
    dio.interceptors.add(
      PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true,
      ),
    );
    return dio;
  }

  static Future<bool> hasConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      print('not connected');
      return false;
    }
  }

}
