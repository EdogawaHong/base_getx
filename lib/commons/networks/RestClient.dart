import 'package:base/model/base/account.dart';
import 'package:base/model/response/res_login.dart';
import 'package:retrofit/http.dart';
import 'package:dio/dio.dart';

part 'RestClient.g.dart';

@RestApi()
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST("api/Account/UserLogin")
  Future<ResLogin> login(@Body() AccountLogin userLoginRQ);
}