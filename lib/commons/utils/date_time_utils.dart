import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DateTimeUtils {
  static Future<DateTime> showDatePickerDialog(BuildContext context,
      {DateTime? initialDateValue}) async {
    DateTime date = (await showDatePicker(
          context: context,
          locale: Locale('vi', 'VI'),
          initialDate: initialDateValue ?? DateTime.now(),
          firstDate: DateTime(2000),
          lastDate: DateTime(3000),
          builder: (context, Widget? child) {
            return Theme(
              data: ThemeData.light(),
              child: child!,
            );
          },
        )) ??
        (initialDateValue ?? DateTime.now());
    return date;
  }
}
