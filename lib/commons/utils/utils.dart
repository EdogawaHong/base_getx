import 'package:base/commons/utils/shared_pref.dart';
import 'package:base/ui/login/ui/login_ui.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class Utils{
  static void forceLogout() async {
    SharedPref.clear().whenComplete(() {
      Get.offAll(LoginUI());
    });
  }

  static Widget getSpaceView(double width, double height) {
    return SizedBox(
      width: width,
      height: height,
    );
  }

  static showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        timeInSecForIosWeb: 10,
        fontSize: 16.0);
  }
}