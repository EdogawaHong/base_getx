import 'package:intl/intl.dart';

class TimeUtils {
  static String dateFormat = "dd/MM/yyyy";

  static DateTime getFirstDateOfMonth() {
    DateTime now = DateTime.now();
    DateTime firstDayOfMonth = new DateTime(now.year, now.month, 1);
    print("${firstDayOfMonth.month}/${firstDayOfMonth.day}");
    return firstDayOfMonth;
  }

  static DateTime getLastDateOfMonth() {
    DateTime now = DateTime.now();
    DateTime lastDayOfMonth = new DateTime(now.year, now.month + 1, 0);
    print("${lastDayOfMonth.month}/${lastDayOfMonth.day}");
    return lastDayOfMonth;
  }

  static DateTime convertStringToDate(String data, String formatFrom) {
    return new DateFormat(formatFrom).parse(data);
  }

  static String convertTimeToFormat(
      String data,
      String formatFrom,
      String formatTo,
      ) {
    DateTime dateTime = new DateFormat(formatFrom).parse(data);
    String formattedDate = DateFormat(formatTo).format(dateTime);
    return formattedDate;
  }

  static String convertDateTimeToFormat(DateTime time, String formatTo) {
    String formattedDate = DateFormat(formatTo).format(time);
    return formattedDate;
  }

  static String getFirstDayOfMonth(DateTime time, String formatTo) {
    DateTime now = time;
    DateTime firstDayOfMonth = new DateTime(now.year, now.month, 1);
    print("${firstDayOfMonth.month}/${firstDayOfMonth.day}");
    return convertDateTimeToFormat(firstDayOfMonth, formatTo);
  }

  static String getLastDayOfMonth(DateTime time, String formatTo) {
    DateTime now = time;
    DateTime lastDayOfMonth = new DateTime(now.year, now.month + 1, 0);
    print("${lastDayOfMonth.month}/${lastDayOfMonth.day}");
    return convertDateTimeToFormat(lastDayOfMonth, formatTo);
  }

  static String getFirstTimeOfDay() {
    DateTime now = DateTime.now();
    String firstTimeOfDay =
        "${(now.day.toString().length > 1) ? now.day : "0${now.day}"}-"
        "${(now.month.toString().length > 1) ? now.month : "0${now.month}"}-"
        "${now.year} 00:00";
    return firstTimeOfDay;
  }

  static DateTime getFirstDayOfWeek() {
    DateTime now = DateTime.now();
    int currentDay = now.weekday;
    DateTime firstDayOfWeek = now.subtract(Duration(days: currentDay - 1));
    return firstDayOfWeek;
  }

  static String getYesterday(DateTime time, String formatTo) {
    DateTime now = time;
    DateTime yesterday = new DateTime(
        now.year, now.month, now.day - 1, now.hour, now.minute, now.second);
    print("${yesterday.month}/${yesterday.day}");
    print(convertDateTimeToFormat(yesterday, formatTo));
    return convertDateTimeToFormat(yesterday, formatTo);
  }

  static weekday(DateTime date){
    int index=date.weekday;
    String name='';
    switch(index){
      case 1: name='Thứ hai';break;
      case 2: name='Thứ ba';break;
      case 3: name='Thứ tư';break;
      case 4: name='Thứ năm';break;
      case 5: name='Thứ sáu';break;
      case 6: name='Thứ bảy';break;
      case 7: name='Chủ nhật';break;
      default: name='';break;
    }
    return name;
  }

}
