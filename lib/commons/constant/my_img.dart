import 'package:base/commons/constant/size_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyImage{
  static Widget imageNetwork(
      String url,
      double w,
      double h,
      BoxFit boxFit,
      double radius,
      Color colorBorder,
      ) {
    return Container(
      height: h,
      width: w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: colorBorder,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: Image.network(
          url,
          width: w,
          height: h,
          fit: boxFit,
          errorBuilder: (
              BuildContext context,
              Object? exception,
              StackTrace? stackTrace,
              ) {
            return Container(
              color: Colors.grey[300],
              child: Center(
                child: Icon(
                  Icons.collections_rounded,
                  color: Colors.grey,
                  size: SizeConst.img35,
                ),
              ),
            );
          },
          loadingBuilder: (BuildContext ctx, Widget? child,
              ImageChunkEvent? loadingProgress) {
            if (loadingProgress == null) {
              return child!;
            } else {
              return Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}