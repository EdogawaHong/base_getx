import 'package:base/commons/constant/colors_const.dart';
import 'package:base/commons/constant/size_const.dart';
import 'package:flutter/cupertino.dart';

class AppConst{
  static TextStyle style=TextStyle(fontSize: SizeConst.size18,color: ColorsConst.colorPrimary);
}