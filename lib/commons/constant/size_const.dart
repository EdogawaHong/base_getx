import 'package:base/commons/utils/size_utils.dart';

class SizeConst {
  //add height
  static double h320 = SizeUtil.setHeight(320);

  // add width
  static double w100 = SizeUtil.setWidth(100);

  //use for  fontSize
  static double size18 = SizeUtil.setFontText(18);

  //add font text img
  static double img35 = SizeUtil.setFontText(35);

  // use for padding and margin
  static double pad16 = SizeUtil.setPadding(16);

  //add radius
  static double r10 = SizeUtil.setRadius(10);

}
