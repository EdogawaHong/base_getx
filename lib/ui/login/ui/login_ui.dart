import 'package:base/commons/constant/app_const.dart';
import 'package:base/commons/constant/img_const.dart';
import 'package:base/commons/constant/size_const.dart';
import 'package:base/commons/utils/utils.dart';
import 'package:base/ui/login/controller/login_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginUI extends StatelessWidget {
  LoginController loginController=Get.put(LoginController());
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(ImgConst.bg), fit: BoxFit.fill)),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body:Obx((){
              return Stack(
                children: [
                  Center(
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: SizeConst.pad16,
                          horizontal: SizeConst.pad16),
                      margin: EdgeInsets.symmetric(
                          vertical: SizeConst.pad16,
                          horizontal: SizeConst.pad16),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(SizeConst.r10),
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          TextFormField(
                            controller: loginController.nameController,
                            style: AppConst.style,
                            decoration: InputDecoration(
                                hintText: "Nhập tên tài khoản"),
                          ),
                          TextFormField(
                            controller: loginController.passController,
                            style: AppConst.style,
                            decoration:
                            InputDecoration(hintText: "Nhập mật khẩu"),
                          ),
                          Utils.getSpaceView(0, 16),
                          InkWell(
                            onTap: () {
                              loginController.login();
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 32),
                              color: Colors.blueAccent,
                              child: Text("Đăng nhập"),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child: (loginController.isLoading.value)
                        ? CircularProgressIndicator()
                        : Container(),
                  )
                ],
              );
            })
        ));
  }
}
