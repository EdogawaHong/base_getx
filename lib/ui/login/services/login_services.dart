import 'package:base/commons/constant/string_const.dart';
import 'package:base/commons/networks/network_utils.dart';
import 'package:base/commons/networks/RestClient.dart';
import 'package:base/commons/utils/utils.dart';
import 'package:base/model/base/account.dart';
import 'package:base/model/response/res_login.dart';

class LoginService{

  static ResLogin? resLogin;

  static Future<ResLogin?> login({String? valUsername, String? valPassword}) async {
       AccountLogin userLoginRQ = AccountLogin(
      userName: valUsername,
      passward: valPassword,
    );
    final client = RestClient(NetworkUtils.getDioClient());
    bool checkNetwork = await NetworkUtils.hasConnection();
    if (checkNetwork) {
      await client.login(userLoginRQ).then((value) {
        print("login 1: ${value.toJson()}");
        resLogin=value;
        return value;
      }).catchError((onError) {
        Utils.showToast(StringConst.error);
        print('login : ${onError.toString()}');
        resLogin=null;
        return null;
      });
    } else {
      Utils.showToast(StringConst.noInternet);
      resLogin= null;
      return null;
    }
  }
}