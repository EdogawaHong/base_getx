import 'package:base/model/base/account.dart';
import 'package:base/model/response/res_login.dart';
import 'package:base/ui/login/services/login_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  TextEditingController nameController = TextEditingController();
  TextEditingController passController = TextEditingController();
  var isLoading = false.obs;
  var resLogin=ResLogin().obs;

  @override
  void onInit() {
    super.onInit();
  }

  void login() async {
    try {
      isLoading(true);
      update();
      LoginService.login(
              valUsername: nameController.text.trim(),
              valPassword: passController.text.trim())
          .then((value) {
        if(value!=null){
          resLogin.value = value;
          print("login: ${resLogin.value.toJson()}");
          if (resLogin.value != null) {
            print("login: ${resLogin.value.toJson()}");
          } else {
            print('login null');
          }
          update();
        }
      });
    } finally {
      isLoading(false);
      update();
    }
  }
}
