class AccountLogin {
  String? userName;
  String? passward;

  AccountLogin({this.userName, this.passward});

  AccountLogin.fromJson(Map<String, dynamic> json) {
    userName = json['UserName'];
    passward = json['Passward'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserName'] = this.userName;
    data['Passward'] = this.passward;
    return data;
  }
}